## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Calix Developer Portal. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Calix Developer Portal.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Calix_cloud_devportal. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getCalloutcomeSubscriberCallOutcome(subscriberLocationId, extref, callback)</td>
    <td style="padding:15px">Get call outcome data</td>
    <td style="padding:15px">{base_path}/{version}/calloutcome/subscriber/call-outcome?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCaleaAccessLogs(subscriberId, clientMACAddress, startDate, endDate, callback)</td>
    <td style="padding:15px">Get the Hotspot access logs by subscriber ID or client MAC address.</td>
    <td style="padding:15px">{base_path}/{version}/calea/access-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetPerfTestCafIITest(name, callback)</td>
    <td style="padding:15px">Retrieve all existing CAF II tests, or a single test instance by orgId and name, or for a given dat</td>
    <td style="padding:15px">{base_path}/{version}/net-perf-test/cafIITest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetPerfTestCafIITest(body, callback)</td>
    <td style="padding:15px">Create a new CAF II Test instance</td>
    <td style="padding:15px">{base_path}/{version}/net-perf-test/cafIITest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetPerfTestCafIITestId(id, callback)</td>
    <td style="padding:15px">Retrieve an existing CAF II test by _id string</td>
    <td style="padding:15px">{base_path}/{version}/net-perf-test/cafIITest/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetPerfTestCafIITestId(id, body, callback)</td>
    <td style="padding:15px">Update an existing test</td>
    <td style="padding:15px">{base_path}/{version}/net-perf-test/cafIITest/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetPerfTestCafIITestId(id, callback)</td>
    <td style="padding:15px">Delete an existing CAF II test by _id string</td>
    <td style="padding:15px">{base_path}/{version}/net-perf-test/cafIITest/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetPerfTestCafIITestIdSuspend(id, callback)</td>
    <td style="padding:15px">Suspend an existing test</td>
    <td style="padding:15px">{base_path}/{version}/net-perf-test/cafIITest/{pathv1}/suspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetPerfTestCafIITestIdResume(id, callback)</td>
    <td style="padding:15px">Resume an existing test</td>
    <td style="padding:15px">{base_path}/{version}/net-perf-test/cafIITest/{pathv1}/resume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetPerfTestOoklaServer(country = 'US', callback)</td>
    <td style="padding:15px">Retrieve all Ookla servers</td>
    <td style="padding:15px">{base_path}/{version}/net-perf-test/ooklaServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetPerfTestTestResultZipExternal(testId, callback)</td>
    <td style="padding:15px">Retrieve both Speed Test and Latency Test results as a zip file</td>
    <td style="padding:15px">{base_path}/{version}/net-perf-test/testResult/zip/external?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribers(devices, account, phone, email, region, billingStatus = 'Active', withDevices, withServices, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve all subscriber(s) that match all the given query criteria</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBillingSubscribers(body, callback)</td>
    <td style="padding:15px">Create a new subscriber.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberId(subscriberId, includeDeviceData, includeDecommissionedDevices, callback)</td>
    <td style="padding:15px">Retrieve an existing subscriber by subscriberId</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSubscribersSubscriberId(subscriberId, body, callback)</td>
    <td style="padding:15px">Update an existing subscriber by subscriberId.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBillingSubscribersSubscriberId(subscriberId, callback)</td>
    <td style="padding:15px">Delete an existing subscriber by subscriberId</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdDevices(subscriberId, callback)</td>
    <td style="padding:15px">Retrieve associated device list for a subscriber location</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSubscribersSubscriberIdDevices(subscriberId, deviceList, callback)</td>
    <td style="padding:15px">Update associated device list</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBillingSubscribersSubscriberIdDevices(subscriberId, deviceId, callback)</td>
    <td style="padding:15px">Add a device id to an existing subscriber's associated device list</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSubscribersSubscriberIdDevicesDeviceId(subscriberId, deviceId, newDeviceId, callback)</td>
    <td style="padding:15px">Replace a device ID in an exsiting subscriber's associated device list with another device ID</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBillingSubscribersSubscriberIdDevicesDeviceId(subscriberId, deviceId, callback)</td>
    <td style="padding:15px">Delete a device from an existing subscriber's associated device list</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdFccIdentifiers(subscriberId, callback)</td>
    <td style="padding:15px">Retrieve an existing FCC identifier by subscriberId.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/fccIdentifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSubscribersSubscriberIdFccIdentifiers(subscriberId, body, callback)</td>
    <td style="padding:15px">Update an existing FCC identifier by subscriberId.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/fccIdentifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBillingSubscribersSubscriberIdFccIdentifiers(subscriberId, body, callback)</td>
    <td style="padding:15px">Create a new FCC identifier for the subscriber.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/fccIdentifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBillingSubscribersSubscriberIdFccIdentifiers(subscriberId, callback)</td>
    <td style="padding:15px">Delete an existing FCC identifier by subscriberId.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/fccIdentifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdServices(subscriberId, summaryOnly, callback)</td>
    <td style="padding:15px">Retrieve service list for a subscriber location</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSubscribersSubscriberIdServices(subscriberId, ignoreDuplicates, body, callback)</td>
    <td style="padding:15px">Update the entire service list for a subscriber location</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBillingSubscribersSubscriberIdServices(subscriberId, body, callback)</td>
    <td style="padding:15px">Add a new service to a subscriber location</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdServicesServiceId(subscriberId, serviceId, callback)</td>
    <td style="padding:15px">Retrieve an existing service instance for a subscriber location by service instance id</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSubscribersSubscriberIdServicesServiceId(subscriberId, serviceId, body, callback)</td>
    <td style="padding:15px">Update an existing service instance for a subscriber location by service instance id.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBillingSubscribersSubscriberIdServicesServiceId(subscriberId, serviceId, callback)</td>
    <td style="padding:15px">Delete an existing service instance from a subscriber location by service instance id</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdServicesStatus(subscriberId, callback)</td>
    <td style="padding:15px">Status of all existing service(s) for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/services/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdAppServices(subscriberId, callback)</td>
    <td style="padding:15px">Retrieve all existing application services for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/appServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdAppServicesAppServiceName(subscriberId, appServiceName = 'ProtectIQ', callback)</td>
    <td style="padding:15px">Retrieve an existing application service setting by subscriberId and service name</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/appServices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSubscribersSubscriberIdAppServicesAppServiceName(subscriberId, appServiceName = 'ProtectIQ', body, callback)</td>
    <td style="padding:15px">Create a new application service setting or update an existing application service setting for a su</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/appServices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdArloService(subscriberId, callback)</td>
    <td style="padding:15px">Retrieve the Arlo Service Info for a subscriber.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/arlo-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSubscribersSubscriberIdArloService(subscriberId, body, callback)</td>
    <td style="padding:15px">Update the Arlo Service for a subsccriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/arlo-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBillingSubscribersSubscriberIdArloService(subscriberId, body, callback)</td>
    <td style="padding:15px">Create the Arlo Service for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/arlo-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBillingSubscribersSubscriberIdArloService(subscriberId, callback)</td>
    <td style="padding:15px">Delete the Arlo Service for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/arlo-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdServifyContract(subscriberId, callback)</td>
    <td style="padding:15px">Retrieve the Servify Contract for a subscriber.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/servify-contract?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBillingSubscribersSubscriberIdServifyContract(subscriberId, body, callback)</td>
    <td style="padding:15px">Create Servify Contract for one subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/servify-contract?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBillingSubscribersSubscriberIdServifyContract(subscriberId, reasonCode, callback)</td>
    <td style="padding:15px">Delete the Servify Contract for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/servify-contract?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdManagedServiceArlo(subscriberId, callback)</td>
    <td style="padding:15px">Retrieve the Arlo Service Info for a subscriber.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/managed-service/arlo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSubscribersSubscriberIdManagedServiceArlo(subscriberId, body, callback)</td>
    <td style="padding:15px">Update the Arlo Service for a subsccriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/managed-service/arlo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBillingSubscribersSubscriberIdManagedServiceArlo(subscriberId, body, callback)</td>
    <td style="padding:15px">Create the Arlo Service for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/managed-service/arlo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBillingSubscribersSubscriberIdManagedServiceArlo(subscriberId, callback)</td>
    <td style="padding:15px">Delete the Arlo Service for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/managed-service/arlo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdManagedServiceServify(subscriberId, callback)</td>
    <td style="padding:15px">Retrieve the Servify Contract for a subscriber.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/managed-service/servify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSubscribersSubscriberIdManagedServiceServify(subscriberId, planCode = 'SERVIFYCAREBRONZE', callback)</td>
    <td style="padding:15px">Update Servify Contract Plan for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/managed-service/servify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBillingSubscribersSubscriberIdManagedServiceServify(subscriberId, body, callback)</td>
    <td style="padding:15px">Create Servify Contract for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/managed-service/servify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBillingSubscribersSubscriberIdManagedServiceServify(subscriberId, reasonCode, callback)</td>
    <td style="padding:15px">Delete the Servify Contract for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/managed-service/servify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSubscribersSubscriberIdSmarttownWifiSubscriber(subscriberId, callback)</td>
    <td style="padding:15px">Retrieve the subscriber membership of SmartTown Wi-Fi for a subscriber.</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/smarttown-wifi/subscriber?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSubscribersSubscriberIdSmarttownWifiSubscriber(subscriberId, body, callback)</td>
    <td style="padding:15px">Create/Update subscriber membershp of SmartTown Wi-Fi for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/billing/subscribers/{pathv1}/smarttown-wifi/subscriber?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingServiceDefinitions(callback)</td>
    <td style="padding:15px">Retrieve all service definition(s)</td>
    <td style="padding:15px">{base_path}/{version}/billing/serviceDefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingServiceDefinitions(body, callback)</td>
    <td style="padding:15px">Update all service definition(s) via a single request</td>
    <td style="padding:15px">{base_path}/{version}/billing/serviceDefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBillingServiceDefinitions(body, callback)</td>
    <td style="padding:15px">Create a new service definition.</td>
    <td style="padding:15px">{base_path}/{version}/billing/serviceDefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingServiceDefinitionsServiceDefinitionId(serviceDefinitionId, callback)</td>
    <td style="padding:15px">Retrieve an existing service definition by usoc.</td>
    <td style="padding:15px">{base_path}/{version}/billing/serviceDefinitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingServiceDefinitionsServiceDefinitionId(serviceDefinitionId, body, callback)</td>
    <td style="padding:15px">Update an existing service definition by id.</td>
    <td style="padding:15px">{base_path}/{version}/billing/serviceDefinitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBillingServiceDefinitionsServiceDefinitionId(serviceDefinitionId, callback)</td>
    <td style="padding:15px">Delete an existing service definition by usoc.</td>
    <td style="padding:15px">{base_path}/{version}/billing/serviceDefinitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSupportOperatorSsid(serialNumber, subscriberId, callback)</td>
    <td style="padding:15px">Retrieve ssid by subscriberId or RG serialNumber</td>
    <td style="padding:15px">{base_path}/{version}/billing/support/operator/ssid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSupportOperatorSsid(serialNumber, subscriberId, body, callback)</td>
    <td style="padding:15px">Update ssid by subscriberId or RG serialNumber</td>
    <td style="padding:15px">{base_path}/{version}/billing/support/operator/ssid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSupportOperatorSecondarynetworkAvailability(serialNumber, subscriberId, callback)</td>
    <td style="padding:15px">Retrieve secondarynetwork availability by subscriberId or RG serialNumber</td>
    <td style="padding:15px">{base_path}/{version}/billing/support/operator/secondarynetwork/availability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBillingSupportOperatorSecondarynetwork(serialNumber, subscriberId, callback)</td>
    <td style="padding:15px">Retrieve secondarynetwork by subscriberId or RG serialNumber</td>
    <td style="padding:15px">{base_path}/{version}/billing/support/operator/secondarynetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBillingSupportOperatorSecondarynetwork(serialNumber, subscriberId, body, callback)</td>
    <td style="padding:15px">Update secondarynetwork by subscriberId or RG serialNumber</td>
    <td style="padding:15px">{base_path}/{version}/billing/support/operator/secondarynetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBillingSupportOperatorSecondarynetwork(serialNumber, subscriberId, body, callback)</td>
    <td style="padding:15px">Create secondarynetwork by subscriberId or RG serialNumber</td>
    <td style="padding:15px">{base_path}/{version}/billing/support/operator/secondarynetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBillingSupportOperatorSecondarynetwork(serialNumber, subscriberId, body, callback)</td>
    <td style="padding:15px">Delete secondarynetwork by subscriberId or RG serialNumber</td>
    <td style="padding:15px">{base_path}/{version}/billing/support/operator/secondarynetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchServiceDefinitions(name, serviceType = 'DATA', callback)</td>
    <td style="padding:15px">GET method for retrieving details of all service definitions</td>
    <td style="padding:15px">{base_path}/{version}/billing/e2e/serviceDefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficUsage(granularity, startTime, endTime, subscriber, callback)</td>
    <td style="padding:15px">Get subscriber traffic usage</td>
    <td style="padding:15px">{base_path}/{version}/insights/subscribers/{pathv1}/trafficusage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficUsageForAllSubs(granularity, startTime, endTime, offset, limit, callback)</td>
    <td style="padding:15px">Get subscriber traffic usage for all subscribers</td>
    <td style="padding:15px">{base_path}/{version}/insights/subscriberstrafficusage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
