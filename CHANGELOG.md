
## 0.3.4 [10-14-2024]

* Changes made at 2024.10.14_19:36PM

See merge request itentialopensource/adapters/adapter-calix_cloud_devportal!13

---

## 0.3.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-calix_cloud_devportal!11

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_17:39PM

See merge request itentialopensource/adapters/adapter-calix_cloud_devportal!10

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_18:36PM

See merge request itentialopensource/adapters/adapter-calix_cloud_devportal!9

---

## 0.3.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-calix_cloud_devportal!8

---

## 0.2.4 [03-28-2024]

* Changes made at 2024.03.28_13:08PM

See merge request itentialopensource/adapters/cloud/adapter-calix_cloud_devportal!7

---

## 0.2.3 [03-11-2024]

* Changes made at 2024.03.11_15:27PM

See merge request itentialopensource/adapters/cloud/adapter-calix_cloud_devportal!6

---

## 0.2.2 [02-28-2024]

* Changes made at 2024.02.28_11:37AM

See merge request itentialopensource/adapters/cloud/adapter-calix_cloud_devportal!5

---

## 0.2.1 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-calix_cloud_devportal!4

---

## 0.2.0 [11-10-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-calix_cloud_devportal!3

---

## 0.1.1 [04-03-2023]

* Bug fixes and performance improvements

See commit 6b92cc9

---
