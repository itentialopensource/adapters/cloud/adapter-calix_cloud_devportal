# Calix Developer Portal

Vendor: Calix
Homepage: https://www.calix.com/

Product: Developer Portal
Product Page: https://www.calix.com/platforms/calix_cloud/operations-cloud.html

## Introduction
We classify Calix Developer Portal into the Cloud domain as it it enables the interaction with and management of Calix Cloud services. 

## Why Integrate
The Calix Developer Portal adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Calix Developer Portal. With this adapter you have the ability to perform operations such as:

- Create, retrieve and update billing subscribers.
- Retrieve insights and data on subscriber traffic usage.
- Get Subscriber Devices
- Get Subscriber Services
- Get Subscriber FCC Identifier

## Additional Product Documentation
The [API documents for Calix Developer Portal]()

