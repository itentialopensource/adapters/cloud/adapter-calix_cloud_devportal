# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Calix_cloud_devportal System. The API that was used to build the adapter for Calix_cloud_devportal is usually available in the report directory of this adapter. The adapter utilizes the Calix_cloud_devportal API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Calix Developer Portal adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Calix Developer Portal. With this adapter you have the ability to perform operations such as:

- Create, retrieve and update billing subscribers.
- Retrieve insights and data on subscriber traffic usage.
- Get Subscriber Devices
- Get Subscriber Services
- Get Subscriber FCC Identifier

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
