/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-calix_cloud_devportal',
      type: 'CalixCloudDevportal',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const CalixCloudDevportal = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Calix_cloud_devportal Adapter Test', () => {
  describe('CalixCloudDevportal Class Tests', () => {
    const a = new CalixCloudDevportal(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const callOutcomeSubscriberLocationId = 'fakedata';
    const callOutcomeExtref = 'fakedata';
    describe('#getCalloutcomeSubscriberCallOutcome - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCalloutcomeSubscriberCallOutcome(callOutcomeSubscriberLocationId, callOutcomeExtref, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CallOutcome', 'getCalloutcomeSubscriberCallOutcome', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCaleaAccessLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCaleaAccessLogs(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessLogs', 'getCaleaAccessLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netPerfTestPostNetPerfTestCafIITestBodyParam = {
      name: 'CA-2019-Q1-Gold-Internet',
      timezone: 'America/Chicago',
      startDate: 'string',
      numberOfDays: 5,
      devices: [
        'CXNK00112233',
        'CXNK004E6DEF'
      ]
    };
    describe('#postNetPerfTestCafIITest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNetPerfTestCafIITest(netPerfTestPostNetPerfTestCafIITestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetPerfTest', 'postNetPerfTestCafIITest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetPerfTestCafIITest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetPerfTestCafIITest(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetPerfTest', 'getNetPerfTestCafIITest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netPerfTestId = 'fakedata';
    const netPerfTestPutNetPerfTestCafIITestIdBodyParam = {
      name: 'CA_10M/1M',
      description: '2019 Q1 Northern California Gold Internet Subscribers',
      timezone: 'America/Chicago',
      primaryOoklaServerId: '1234',
      speedThreshold: 80,
      latencyThreshold: 10,
      serviceTierDownloadSpeed: 1000000,
      serviceTierUploadSpeed: 1000000,
      downloadUrl: 'http://9.9.9.9/download/10M',
      uploadUrl: 'http://9.9.9.9/upload',
      uploadFileSize: 1000000,
      startDate: '2018-12-12',
      numberOfDays: 7,
      startHour: 18,
      numberOfHours: 6,
      devices: [
        'CXNK00112233',
        'CXNK004E6DEF'
      ]
    };
    describe('#putNetPerfTestCafIITestId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putNetPerfTestCafIITestId(netPerfTestId, netPerfTestPutNetPerfTestCafIITestIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetPerfTest', 'putNetPerfTestCafIITestId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetPerfTestCafIITestId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetPerfTestCafIITestId(netPerfTestId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetPerfTest', 'getNetPerfTestCafIITestId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNetPerfTestCafIITestIdResume - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putNetPerfTestCafIITestIdResume(netPerfTestId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetPerfTest', 'putNetPerfTestCafIITestIdResume', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNetPerfTestCafIITestIdSuspend - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putNetPerfTestCafIITestIdSuspend(netPerfTestId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetPerfTest', 'putNetPerfTestCafIITestIdSuspend', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetPerfTestOoklaServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetPerfTestOoklaServer(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetPerfTest', 'getNetPerfTestOoklaServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netPerfTestTestId = 'fakedata';
    describe('#getNetPerfTestTestResultZipExternal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetPerfTestTestResultZipExternal(netPerfTestTestId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetPerfTest', 'getNetPerfTestTestResultZipExternal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberPostBillingSubscribersBodyParam = {
      subscriberLocationId: '11152501:123 Main St',
      account: '11152500',
      name: 'Andrew Green',
      firstName: 'Andrew',
      lastName: 'Green',
      phone: '101-234-6673',
      email: 'abc@hotmail.com',
      serviceAddress: '123 Main St, San Jose, CA USA',
      street: '1900 Country Dr',
      streetLine2: 'APT 201 3rd Building',
      city: 'Grayslake',
      state: 'IL',
      postcode: '60030',
      country: 'USA',
      billingAddress: 'PO Box 123 San Jose, CA 12345',
      billingStreet: '1900 Country Dr',
      billingStreetLine2: 'APT 201 3rd Building',
      billingCity: 'Grayslake',
      billingState: 'IL',
      billingPostcode: '60030',
      billingCountry: 'USA',
      billingStatus: 'Disconnected',
      region: 'Bay Area',
      location: 'Financial District in San Francisco',
      lat: 45.468388,
      lon: -132.33104,
      customerType: 'Residential',
      attainableRate: 3,
      optout: false,
      field1: 'string',
      field2: 'string',
      field3: 'string',
      field4: 'string',
      field5: 'string',
      highValue: true
    };
    describe('#postBillingSubscribers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBillingSubscribers(subscriberPostBillingSubscribersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'postBillingSubscribers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribers(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'getBillingSubscribers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberSubscriberId = 'fakedata';
    const subscriberPutBillingSubscribersSubscriberIdBodyParam = {
      subscriberLocationId: '11152501:123 Main St',
      account: '11152500',
      name: 'Andrew Green',
      firstName: 'Andrew',
      lastName: 'Green',
      phone: '101-234-6673',
      email: 'abc@hotmail.com',
      serviceAddress: '123 Main St, San Jose, CA USA',
      street: '1900 Country Dr',
      streetLine2: 'APT 201 3rd Building',
      city: 'Grayslake',
      state: 'IL',
      postcode: '60030',
      country: 'USA',
      billingAddress: 'PO Box 123 San Jose, CA 12345',
      billingStreet: '1900 Country Dr',
      billingStreetLine2: 'APT 201 3rd Building',
      billingCity: 'Grayslake',
      billingState: 'IL',
      billingPostcode: '60030',
      billingCountry: 'USA',
      billingStatus: 'Active',
      region: 'Bay Area',
      location: 'Financial District in San Francisco',
      lat: 45.468388,
      lon: -132.33104,
      customerType: 'Business',
      attainableRate: 6,
      optout: true,
      field1: 'string',
      field2: 'string',
      field3: 'string',
      field4: 'string',
      field5: 'string',
      highValue: false
    };
    describe('#putBillingSubscribersSubscriberId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSubscribersSubscriberId(subscriberSubscriberId, subscriberPutBillingSubscribersSubscriberIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'putBillingSubscribersSubscriberId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberId(subscriberSubscriberId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'getBillingSubscribersSubscriberId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberDevicesSubscriberId = 'fakedata';
    const subscriberDevicesDeviceId = 'fakedata';
    describe('#postBillingSubscribersSubscriberIdDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBillingSubscribersSubscriberIdDevices(subscriberDevicesSubscriberId, subscriberDevicesDeviceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberDevices', 'postBillingSubscribersSubscriberIdDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberDevicesDeviceList = 'fakedata';
    describe('#putBillingSubscribersSubscriberIdDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSubscribersSubscriberIdDevices(subscriberDevicesSubscriberId, subscriberDevicesDeviceList, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberDevices', 'putBillingSubscribersSubscriberIdDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberIdDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdDevices(subscriberDevicesSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberDevices', 'getBillingSubscribersSubscriberIdDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberDevicesNewDeviceId = 'fakedata';
    describe('#putBillingSubscribersSubscriberIdDevicesDeviceId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSubscribersSubscriberIdDevicesDeviceId(subscriberDevicesSubscriberId, subscriberDevicesDeviceId, subscriberDevicesNewDeviceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberDevices', 'putBillingSubscribersSubscriberIdDevicesDeviceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberFCCIdentifierSubscriberId = 'fakedata';
    const subscriberFCCIdentifierPostBillingSubscribersSubscriberIdFccIdentifiersBodyParam = {
      hubbLocationId: 'A12345',
      fccSubscriberId: 'A-800098678'
    };
    describe('#postBillingSubscribersSubscriberIdFccIdentifiers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBillingSubscribersSubscriberIdFccIdentifiers(subscriberFCCIdentifierSubscriberId, subscriberFCCIdentifierPostBillingSubscribersSubscriberIdFccIdentifiersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberFCCIdentifier', 'postBillingSubscribersSubscriberIdFccIdentifiers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberFCCIdentifierPutBillingSubscribersSubscriberIdFccIdentifiersBodyParam = {
      hubbLocationId: 'A12345',
      fccSubscriberId: 'A-800098678'
    };
    describe('#putBillingSubscribersSubscriberIdFccIdentifiers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSubscribersSubscriberIdFccIdentifiers(subscriberFCCIdentifierSubscriberId, subscriberFCCIdentifierPutBillingSubscribersSubscriberIdFccIdentifiersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberFCCIdentifier', 'putBillingSubscribersSubscriberIdFccIdentifiers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberIdFccIdentifiers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdFccIdentifiers(subscriberFCCIdentifierSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberFCCIdentifier', 'getBillingSubscribersSubscriberIdFccIdentifiers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberServicesSubscriberId = 'fakedata';
    const subscriberServicesPostBillingSubscribersSubscriberIdServicesBodyParam = {
      usoc: '7030Z759',
      type: 'other',
      description: 'Gold Internet',
      downSpeed: 100,
      upSpeed: 20,
      group: '6-10M',
      techType: 'Fiber',
      customerType: 'Residential',
      endpointMappingOption1: '825-4677',
      endpointMappingOption2: 'string',
      endpointMappingOption3: 'string',
      endpointMappingOption4: 'string',
      activate: false,
      sVlan: 6,
      cVlan: 8,
      ceVlan: 7,
      untagged: true,
      pppoeUsername: 'string',
      pppoePassword: 'string',
      interface: 'string',
      voiceServiceType: 'X_000631_TDMGW',
      dialPlan: 'string',
      sipProfile: 'string',
      h248Profile: 'string',
      multicastProfile: 'string',
      voiceInterfaces: [
        {
          name: 'string',
          enable: false,
          terminationId: 'string',
          sipUri: 'string',
          sipUsername: 'string',
          sipPassword: 'string'
        }
      ],
      staticHostMode: 'L3',
      staticIpAddress: 'string',
      staticNetmask: 'string',
      staticGateway: 'string',
      memberPorts: [
        'g4'
      ]
    };
    describe('#postBillingSubscribersSubscriberIdServices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBillingSubscribersSubscriberIdServices(subscriberServicesSubscriberId, subscriberServicesPostBillingSubscribersSubscriberIdServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberServices', 'postBillingSubscribersSubscriberIdServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberServicesPutBillingSubscribersSubscriberIdServicesBodyParam = [
      {
        usoc: '7030Z759',
        type: 'video',
        description: 'Gold Internet',
        downSpeed: 100,
        upSpeed: 20,
        group: '6-10M',
        techType: 'Fiber',
        customerType: 'Residential',
        endpointMappingOption1: '825-4677',
        endpointMappingOption2: 'string',
        endpointMappingOption3: 'string',
        endpointMappingOption4: 'string',
        activate: false,
        sVlan: 4,
        cVlan: 8,
        ceVlan: 6,
        untagged: false,
        pppoeUsername: 'string',
        pppoePassword: 'string',
        interface: 'string',
        voiceServiceType: 'SIP',
        dialPlan: 'string',
        sipProfile: 'string',
        h248Profile: 'string',
        multicastProfile: 'string',
        voiceInterfaces: [
          {
            name: 'string',
            enable: false,
            terminationId: 'string',
            sipUri: 'string',
            sipUsername: 'string',
            sipPassword: 'string'
          },
          {
            name: 'string',
            enable: false,
            terminationId: 'string',
            sipUri: 'string',
            sipUsername: 'string',
            sipPassword: 'string'
          },
          {
            name: 'string',
            enable: false,
            terminationId: 'string',
            sipUri: 'string',
            sipUsername: 'string',
            sipPassword: 'string'
          },
          {
            name: 'string',
            enable: false,
            terminationId: 'string',
            sipUri: 'string',
            sipUsername: 'string',
            sipPassword: 'string'
          },
          {
            name: 'string',
            enable: false,
            terminationId: 'string',
            sipUri: 'string',
            sipUsername: 'string',
            sipPassword: 'string'
          },
          {
            name: 'string',
            enable: true,
            terminationId: 'string',
            sipUri: 'string',
            sipUsername: 'string',
            sipPassword: 'string'
          },
          {
            name: 'string',
            enable: true,
            terminationId: 'string',
            sipUri: 'string',
            sipUsername: 'string',
            sipPassword: 'string'
          }
        ],
        staticHostMode: 'L2',
        staticIpAddress: 'string',
        staticNetmask: 'string',
        staticGateway: 'string',
        memberPorts: [
          'g1',
          'g1',
          'g1',
          'g2',
          'g2'
        ]
      }
    ];
    describe('#putBillingSubscribersSubscriberIdServices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSubscribersSubscriberIdServices(subscriberServicesSubscriberId, null, subscriberServicesPutBillingSubscribersSubscriberIdServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberServices', 'putBillingSubscribersSubscriberIdServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberIdServices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdServices(subscriberServicesSubscriberId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberServices', 'getBillingSubscribersSubscriberIdServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberIdServicesStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdServicesStatus(subscriberServicesSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberServices', 'getBillingSubscribersSubscriberIdServicesStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberServicesServiceId = 'fakedata';
    const subscriberServicesPutBillingSubscribersSubscriberIdServicesServiceIdBodyParam = {
      usoc: '7030Z759',
      type: 'video',
      description: 'Gold Internet',
      downSpeed: 100,
      upSpeed: 20,
      group: '6-10M',
      techType: 'Fiber',
      customerType: 'Residential',
      endpointMappingOption1: '825-4677',
      endpointMappingOption2: 'string',
      endpointMappingOption3: 'string',
      endpointMappingOption4: 'string',
      activate: true,
      sVlan: 10,
      cVlan: 9,
      ceVlan: 10,
      untagged: true,
      pppoeUsername: 'string',
      pppoePassword: 'string',
      interface: 'string',
      voiceServiceType: 'H.248',
      dialPlan: 'string',
      sipProfile: 'string',
      h248Profile: 'string',
      multicastProfile: 'string',
      voiceInterfaces: [
        {
          name: 'string',
          enable: true,
          terminationId: 'string',
          sipUri: 'string',
          sipUsername: 'string',
          sipPassword: 'string'
        }
      ],
      staticHostMode: 'L2',
      staticIpAddress: 'string',
      staticNetmask: 'string',
      staticGateway: 'string',
      memberPorts: [
        'g1'
      ]
    };
    describe('#putBillingSubscribersSubscriberIdServicesServiceId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSubscribersSubscriberIdServicesServiceId(subscriberServicesSubscriberId, subscriberServicesServiceId, subscriberServicesPutBillingSubscribersSubscriberIdServicesServiceIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberServices', 'putBillingSubscribersSubscriberIdServicesServiceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberIdServicesServiceId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdServicesServiceId(subscriberServicesSubscriberId, subscriberServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberServices', 'getBillingSubscribersSubscriberIdServicesServiceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberAppServicesSubscriberId = 'fakedata';
    describe('#getBillingSubscribersSubscriberIdAppServices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdAppServices(subscriberAppServicesSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberAppServices', 'getBillingSubscribersSubscriberIdAppServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberAppServicesAppServiceName = 'fakedata';
    const subscriberAppServicesPutBillingSubscribersSubscriberIdAppServicesAppServiceNameBodyParam = {
      subscribe: false
    };
    describe('#putBillingSubscribersSubscriberIdAppServicesAppServiceName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSubscribersSubscriberIdAppServicesAppServiceName(subscriberAppServicesSubscriberId, subscriberAppServicesAppServiceName, subscriberAppServicesPutBillingSubscribersSubscriberIdAppServicesAppServiceNameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberAppServices', 'putBillingSubscribersSubscriberIdAppServicesAppServiceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberIdAppServicesAppServiceName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdAppServicesAppServiceName(subscriberAppServicesSubscriberId, subscriberAppServicesAppServiceName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberAppServices', 'getBillingSubscribersSubscriberIdAppServicesAppServiceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberArloServiceDeprecatedSubscriberId = 'fakedata';
    const subscriberArloServiceDeprecatedPostBillingSubscribersSubscriberIdArloServiceBodyParam = {
      email: 'string',
      plan: 'PARTNER_REGULAR_CANADA',
      '2kCameras': 6
    };
    describe('#postBillingSubscribersSubscriberIdArloService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBillingSubscribersSubscriberIdArloService(subscriberArloServiceDeprecatedSubscriberId, subscriberArloServiceDeprecatedPostBillingSubscribersSubscriberIdArloServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberArloServiceDeprecated', 'postBillingSubscribersSubscriberIdArloService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberArloServiceDeprecatedPutBillingSubscribersSubscriberIdArloServiceBodyParam = {
      plan: 'PARTNER_UNLIMITED_PLUS',
      '2kCameras': 3
    };
    describe('#putBillingSubscribersSubscriberIdArloService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSubscribersSubscriberIdArloService(subscriberArloServiceDeprecatedSubscriberId, subscriberArloServiceDeprecatedPutBillingSubscribersSubscriberIdArloServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberArloServiceDeprecated', 'putBillingSubscribersSubscriberIdArloService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberIdArloService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdArloService(subscriberArloServiceDeprecatedSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberArloServiceDeprecated', 'getBillingSubscribersSubscriberIdArloService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberServifyContractDeprecatedSubscriberId = 'fakedata';
    const subscriberServifyContractDeprecatedPostBillingSubscribersSubscriberIdServifyContractBodyParam = {
      email: 'test@test.com',
      firstName: 'Augustine',
      lastName: 'Oh',
      planCode: 'SERVIFYCAREBRONZE',
      address: '2777 Orchard Pkwy',
      city: 'San Jose',
      state: 'California',
      postal: '95131'
    };
    describe('#postBillingSubscribersSubscriberIdServifyContract - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBillingSubscribersSubscriberIdServifyContract(subscriberServifyContractDeprecatedSubscriberId, subscriberServifyContractDeprecatedPostBillingSubscribersSubscriberIdServifyContractBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberServifyContractDeprecated', 'postBillingSubscribersSubscriberIdServifyContract', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberIdServifyContract - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdServifyContract(subscriberServifyContractDeprecatedSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberServifyContractDeprecated', 'getBillingSubscribersSubscriberIdServifyContract', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managedServiceArloSubscriberId = 'fakedata';
    const managedServiceArloPostBillingSubscribersSubscriberIdManagedServiceArloBodyParam = {
      email: 'string',
      planCode: 'PARTNER_UNLIMITED_PLUS_CANADA',
      '2kCameras': 8
    };
    describe('#postBillingSubscribersSubscriberIdManagedServiceArlo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBillingSubscribersSubscriberIdManagedServiceArlo(managedServiceArloSubscriberId, managedServiceArloPostBillingSubscribersSubscriberIdManagedServiceArloBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagedServiceArlo', 'postBillingSubscribersSubscriberIdManagedServiceArlo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managedServiceArloPutBillingSubscribersSubscriberIdManagedServiceArloBodyParam = {
      planCode: 'PARTNER_REGULAR',
      '2kCameras': 1
    };
    describe('#putBillingSubscribersSubscriberIdManagedServiceArlo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSubscribersSubscriberIdManagedServiceArlo(managedServiceArloSubscriberId, managedServiceArloPutBillingSubscribersSubscriberIdManagedServiceArloBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagedServiceArlo', 'putBillingSubscribersSubscriberIdManagedServiceArlo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberIdManagedServiceArlo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdManagedServiceArlo(managedServiceArloSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagedServiceArlo', 'getBillingSubscribersSubscriberIdManagedServiceArlo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managedServiceServifySubscriberId = 'fakedata';
    const managedServiceServifyPostBillingSubscribersSubscriberIdManagedServiceServifyBodyParam = {
      email: 'test@test.com',
      firstName: 'Augustine',
      lastName: 'Oh',
      planCode: 'SERVIFYCAREBRONZE',
      serviceAddress: '2777 Orchard Pkwy',
      city: 'San Jose',
      state: 'California',
      postcode: '95131'
    };
    describe('#postBillingSubscribersSubscriberIdManagedServiceServify - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBillingSubscribersSubscriberIdManagedServiceServify(managedServiceServifySubscriberId, managedServiceServifyPostBillingSubscribersSubscriberIdManagedServiceServifyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagedServiceServify', 'postBillingSubscribersSubscriberIdManagedServiceServify', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managedServiceServifyPlanCode = 'fakedata';
    describe('#putBillingSubscribersSubscriberIdManagedServiceServify - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSubscribersSubscriberIdManagedServiceServify(managedServiceServifySubscriberId, managedServiceServifyPlanCode, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagedServiceServify', 'putBillingSubscribersSubscriberIdManagedServiceServify', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberIdManagedServiceServify - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdManagedServiceServify(managedServiceServifySubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagedServiceServify', 'getBillingSubscribersSubscriberIdManagedServiceServify', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberSmartTownWiFiSubscriberId = 'fakedata';
    const subscriberSmartTownWiFiPutBillingSubscribersSubscriberIdSmarttownWifiSubscriberBodyParam = {
      enable: true,
      communities: [
        {
          micrositeId: '5ed11c29-efe0-45e5-a64a-8dc70d5285d2'
        }
      ]
    };
    describe('#putBillingSubscribersSubscriberIdSmarttownWifiSubscriber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSubscribersSubscriberIdSmarttownWifiSubscriber(subscriberSmartTownWiFiSubscriberId, subscriberSmartTownWiFiPutBillingSubscribersSubscriberIdSmarttownWifiSubscriberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberSmartTownWiFi', 'putBillingSubscribersSubscriberIdSmarttownWifiSubscriber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSubscribersSubscriberIdSmarttownWifiSubscriber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSubscribersSubscriberIdSmarttownWifiSubscriber(subscriberSmartTownWiFiSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberSmartTownWiFi', 'getBillingSubscribersSubscriberIdSmarttownWifiSubscriber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceDefinitionsPostBillingServiceDefinitionsBodyParam = {
      _id: 'b565939f-41e2-489d-b6f3-3a4cb66955c1',
      usoc: '7030Z759',
      type: 'voice',
      description: 'Gold Internet',
      downSpeed: 100,
      upSpeed: 20,
      group: '6-10M',
      techType: 'Fiber',
      customerType: 'Residential',
      fee: 75.99
    };
    describe('#postBillingServiceDefinitions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBillingServiceDefinitions(serviceDefinitionsPostBillingServiceDefinitionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDefinitions', 'postBillingServiceDefinitions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceDefinitionsPutBillingServiceDefinitionsBodyParam = [
      {
        _id: 'b565939f-41e2-489d-b6f3-3a4cb66955c1',
        usoc: '7030Z759',
        type: 'other',
        description: 'Gold Internet',
        downSpeed: 100,
        upSpeed: 20,
        group: '6-10M',
        techType: 'Fiber',
        customerType: 'Residential',
        fee: 75.99
      }
    ];
    describe('#putBillingServiceDefinitions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingServiceDefinitions(serviceDefinitionsPutBillingServiceDefinitionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDefinitions', 'putBillingServiceDefinitions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingServiceDefinitions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingServiceDefinitions((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDefinitions', 'getBillingServiceDefinitions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceDefinitionsServiceDefinitionId = 'fakedata';
    const serviceDefinitionsPutBillingServiceDefinitionsServiceDefinitionIdBodyParam = {
      _id: 'b565939f-41e2-489d-b6f3-3a4cb66955c1',
      usoc: '7030Z759',
      type: 'other',
      description: 'Gold Internet',
      downSpeed: 100,
      upSpeed: 20,
      group: '6-10M',
      techType: 'Fiber',
      customerType: 'Residential',
      fee: 75.99
    };
    describe('#putBillingServiceDefinitionsServiceDefinitionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingServiceDefinitionsServiceDefinitionId(serviceDefinitionsServiceDefinitionId, serviceDefinitionsPutBillingServiceDefinitionsServiceDefinitionIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDefinitions', 'putBillingServiceDefinitionsServiceDefinitionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingServiceDefinitionsServiceDefinitionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingServiceDefinitionsServiceDefinitionId(serviceDefinitionsServiceDefinitionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDefinitions', 'getBillingServiceDefinitionsServiceDefinitionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSIDPostBillingSupportOperatorSecondarynetworkBodyParam = {
      ssid: 'string',
      password: 'string',
      encryptionType: 1,
      type: 2,
      isolated: true,
      smartQos: false,
      eventName: 'string',
      isIndefinite: true,
      duration: {
        startTime: 2,
        endTime: 2
      }
    };
    describe('#postBillingSupportOperatorSecondarynetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBillingSupportOperatorSecondarynetwork(null, null, sSIDPostBillingSupportOperatorSecondarynetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSID', 'postBillingSupportOperatorSecondarynetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSIDPutBillingSupportOperatorSecondarynetworkBodyParam = {
      eventId: 'string',
      ssid: 'string',
      password: 'string',
      encryptionType: 7,
      isolated: false,
      smartQos: false,
      band24Admin: true,
      band5Admin: true,
      band6Admin: false,
      eventName: 'string',
      isIndefinite: false,
      duration: {
        startTime: 1,
        endTime: 2
      }
    };
    describe('#putBillingSupportOperatorSecondarynetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBillingSupportOperatorSecondarynetwork(null, null, sSIDPutBillingSupportOperatorSecondarynetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSID', 'putBillingSupportOperatorSecondarynetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSupportOperatorSecondarynetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBillingSupportOperatorSecondarynetwork(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSID', 'getBillingSupportOperatorSecondarynetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSupportOperatorSecondarynetworkAvailability - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBillingSupportOperatorSecondarynetworkAvailability(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSID', 'getBillingSupportOperatorSecondarynetworkAvailability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSIDPutBillingSupportOperatorSsidBodyParam = {
      id: '1,2'
    };
    describe('#putBillingSupportOperatorSsid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putBillingSupportOperatorSsid(null, null, sSIDPutBillingSupportOperatorSsidBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSID', 'putBillingSupportOperatorSsid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBillingSupportOperatorSsid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBillingSupportOperatorSsid(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSID', 'getBillingSupportOperatorSsid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchServiceDefinitions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchServiceDefinitions(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('E2EServiceDefinitionTemplate', 'searchServiceDefinitions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsGranularity = 'fakedata';
    const reportsStartTime = 'fakedata';
    const reportsSubscriber = 'fakedata';
    describe('#getTrafficUsage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTrafficUsage(reportsGranularity, reportsStartTime, null, reportsSubscriber, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getTrafficUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficUsageForAllSubs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTrafficUsageForAllSubs(reportsGranularity, reportsStartTime, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getTrafficUsageForAllSubs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetPerfTestCafIITestId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetPerfTestCafIITestId(netPerfTestId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetPerfTest', 'deleteNetPerfTestCafIITestId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBillingSubscribersSubscriberId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBillingSubscribersSubscriberId(subscriberSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'deleteBillingSubscribersSubscriberId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBillingSubscribersSubscriberIdDevicesDeviceId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBillingSubscribersSubscriberIdDevicesDeviceId(subscriberDevicesSubscriberId, subscriberDevicesDeviceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberDevices', 'deleteBillingSubscribersSubscriberIdDevicesDeviceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBillingSubscribersSubscriberIdFccIdentifiers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBillingSubscribersSubscriberIdFccIdentifiers(subscriberFCCIdentifierSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberFCCIdentifier', 'deleteBillingSubscribersSubscriberIdFccIdentifiers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBillingSubscribersSubscriberIdServicesServiceId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBillingSubscribersSubscriberIdServicesServiceId(subscriberServicesSubscriberId, subscriberServicesServiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberServices', 'deleteBillingSubscribersSubscriberIdServicesServiceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBillingSubscribersSubscriberIdArloService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBillingSubscribersSubscriberIdArloService(subscriberArloServiceDeprecatedSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberArloServiceDeprecated', 'deleteBillingSubscribersSubscriberIdArloService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberServifyContractDeprecatedReasonCode = 'fakedata';
    describe('#deleteBillingSubscribersSubscriberIdServifyContract - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBillingSubscribersSubscriberIdServifyContract(subscriberServifyContractDeprecatedSubscriberId, subscriberServifyContractDeprecatedReasonCode, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SubscriberServifyContractDeprecated', 'deleteBillingSubscribersSubscriberIdServifyContract', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBillingSubscribersSubscriberIdManagedServiceArlo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBillingSubscribersSubscriberIdManagedServiceArlo(managedServiceArloSubscriberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagedServiceArlo', 'deleteBillingSubscribersSubscriberIdManagedServiceArlo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managedServiceServifyReasonCode = 'fakedata';
    describe('#deleteBillingSubscribersSubscriberIdManagedServiceServify - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBillingSubscribersSubscriberIdManagedServiceServify(managedServiceServifySubscriberId, managedServiceServifyReasonCode, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagedServiceServify', 'deleteBillingSubscribersSubscriberIdManagedServiceServify', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBillingServiceDefinitionsServiceDefinitionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBillingServiceDefinitionsServiceDefinitionId(serviceDefinitionsServiceDefinitionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceDefinitions', 'deleteBillingServiceDefinitionsServiceDefinitionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSIDDeleteBillingSupportOperatorSecondarynetworkBodyParam = {
      eventId: 'string'
    };
    describe('#deleteBillingSupportOperatorSecondarynetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBillingSupportOperatorSecondarynetwork(null, null, sSIDDeleteBillingSupportOperatorSecondarynetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-calix_cloud_devportal-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSID', 'deleteBillingSupportOperatorSecondarynetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
